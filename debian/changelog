ruby-cfpropertylist (3.0.6-1) experimental; urgency=medium

  * Team Upload
  * Move debian/watch to gemwatch.debian.net
  * New upstream version 3.0.6
  * Drop patches applied upstream

 -- Pirate Praveen <praveen@debian.org>  Sun, 16 Apr 2023 17:41:13 +0530

ruby-cfpropertylist (2.2.8-1.2) unstable; urgency=medium

  * Non-maintainer upload

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove unnecessary 'Testsuite: autopkgtest' header.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-cfpropertylist: Add :any qualifier for ruby dependency.
  * Update watch file format version to 4.

  [ Jakob Haufe ]
  * Drop 1.8 compatibility (Closes: #1029726)
  * Ack NMU by Holger Levsen
  * Drop ruby-interpreter dependency
  * Add R³: no
  * Bump Standards-Version
  * Bump to DH 13

 -- Jakob Haufe <sur5r@debian.org>  Fri, 27 Jan 2023 13:25:51 +0100

ruby-cfpropertylist (2.2.8-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 15:28:46 +0100

ruby-cfpropertylist (2.2.8-1) unstable; urgency=medium

  * Initial release (Closes: #773725)

 -- Anish A <aneesh.nl@gmail.com>  Mon, 22 Dec 2014 21:21:38 +0530
